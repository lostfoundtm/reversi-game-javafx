package com.lostfound.reversi.exception;

public class CellFormatException extends RuntimeException {
    public CellFormatException() {
    }

    public CellFormatException(String message) {
        super(message);
    }

    public CellFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public CellFormatException(Throwable cause) {
        super(cause);
    }

    public CellFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
