package com.lostfound.reversi.service;

import com.lostfound.reversi.model.Disk;
import com.lostfound.reversi.model.Move;
import com.lostfound.reversi.model.Target;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.DefaultPropertiesPersister;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@SuppressWarnings("all")
@Service
public class MoveSaveService {

    private static final Logger logger = LoggerFactory.getLogger(MoveSaveService.class);

    private static final String FILE_EXTENTION = ".aim";

    private void increaseProp(Properties props, String key, int defaultValue, int increaseValue) {
        props.setProperty(key, Integer.parseInt(props.getProperty(key, defaultValue + "")) + increaseValue + "");
    }

    private void saveStatistic(String parent, int result) {
        Properties props = new Properties();
        File file = new File(parent, "statistic.properties");
        DefaultPropertiesPersister p = new DefaultPropertiesPersister();
        try {
            file.getParentFile().mkdirs();
            file.createNewFile();
        } catch (IOException e) {
            logger.error("Fail to create file '" + file.getPath() + ". " + e.getMessage());
        }

        try (InputStream in = new FileInputStream(file)) {
            p.load(props, in);
        } catch (IOException e) {
            logger.error("Fail to read statistic from '" + file.getPath() + ". " + e.getMessage());
        }

        increaseProp(props, "played_games", 0, 1);
        if (result == 1) {
            increaseProp(props, "wins", 0, 1);
        } else if (result == -1) {
            increaseProp(props, "loses", 0, 1);
        } else {
            increaseProp(props, "draws", 0, 1);
        }

        try (OutputStream out = new FileOutputStream(file)) {
            p.store(props, out, "Game statistic");
        } catch (IOException e) {
            logger.error("Fail to save statistic to '" + file.getPath() + ". " + e.getMessage());
        }
    }

    public void saveGame(String aiName, Disk color, int width, int height,
                         List<Move> moves, int result) {
        final String parent = "./" + aiName + "/" + width + "x" + height + "/" + color + "/";
        saveStatistic(parent, result);
        for (Move move : moves) {
            saveMove(parent, move, result);
        }
    }

    public void saveMove(String parent, Move move, int result) {
        File file = new File(parent, (move.getWhiteDisks() + move.getBlackDisks()) + "/" + move.getHash() + FILE_EXTENTION);
        file.getParentFile().mkdirs();

        List<Target> savedTargets = null;
        if (file.exists()) {
            savedTargets = readListOfTargets(file);
        } else {
            savedTargets = new ArrayList<>();
        }

        int i = savedTargets.indexOf(move.getTarget());
        if (i > -1) {
            savedTargets.get(i).update(result);
        } else {
            move.getTarget().update(result);
            savedTargets.add(move.getTarget());
        }

        wtiteListOfTarget(file, savedTargets);
    }

    public List<Target> get(String aiName, Disk color, int width, int height, int whiteDisks, int blackDisks, int hash) {
        final String parent = "./" + aiName + "/" + width + "x" + height + "/" + color + "/";
        File file = new File(parent, (whiteDisks + blackDisks) + "/" + hash + FILE_EXTENTION);

        if (file.exists()) {
            return readListOfTargets(file);
        } else {
            return null;
        }
    }

    private List<Target> readListOfTargets(File file) {
        List<Target> list = null;
        try (ObjectInputStream reader = new ObjectInputStream(new FileInputStream(file))) {
            list = (List<Target>) reader.readObject();
        } catch (ClassNotFoundException e) {
            logger.error("Error during read list of targets from '" + file.getPath() +
                    "'. Wrong data was found: " + e.getMessage());
        } catch (IOException e) {
            logger.error("Error during read list of targets from '" + file.getPath() +
                    "'. Something has gone wrong: " + e.getMessage());
        }
        return list;
    }

    private void wtiteListOfTarget(File file, List<Target> list) {
        try (ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(file, false))) {
            writer.writeObject(list);
        } catch (IOException e) {
            logger.error("Error during write list of targets to '" + file.getPath() +
                    "'. Something has gone wrong: " + e.getMessage());
        }
    }
}
