package com.lostfound.reversi.ui;

import com.lostfound.reversi.controller.GameController;
import com.lostfound.reversi.model.Cell;
import com.lostfound.reversi.model.Disk;
import com.lostfound.reversi.model.GameField;
import com.lostfound.reversi.model.GameFieldModel;
import com.lostfound.reversi.player.Player;
import com.lostfound.reversi.player.PlayerFactory;
import com.lostfound.reversi.player.cleverai.alphaomega.LiteConverter;
import com.lostfound.reversi.player.cleverai.alphaomega.LiteGameField;
import com.lostfound.reversi.player.cleverai.alphaomega.LiteGameFieldSupport;
import com.lostfound.reversi.util.GameUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.*;

import static com.lostfound.reversi.util.CommonUtils.getCycled;

public class MainController {

    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @FXML
    private ComboBox<Player> whitePlayerSelector;
    @FXML
    private ComboBox<Player> blackPlayerSelector;
    @FXML
    private Canvas whiteScoreDisk;
    @FXML
    private Canvas blackScoreDisk;
    @FXML
    private Label whiteScore;
    @FXML
    private Label blackScore;
    @FXML
    private Canvas gameFieldCanvas;
    @FXML
    private Button applyButton;
    @FXML
    private Button restartButton;
    @FXML
    private Button startButton;
    @FXML
    private ToggleButton showHideToggleButton;
    @FXML
    private Button skipButton;
    @FXML
    private Button logButton;

    @Autowired
    private PlayerFactory playerFactory;
    @Autowired
    private GameController gameController;
    @Autowired
    private GameFieldModel model;

    @PostConstruct
    private void init() {
        initComponents();
        initGameSettings();
        applyGameSettings();

        initScoreLabels();
        initGameField();

        initEventHandler();
    }

    private void initComponents() {
        gameFieldCanvas.managedProperty().bind(gameFieldCanvas.visibleProperty());
        startButton.managedProperty().bind(startButton.visibleProperty());
        showHideToggleButton.managedProperty().bind(startButton.visibleProperty());
        gameFieldCanvas.setVisible(false);
        showHideToggleButton.setVisible(false);
        showHideToggleButton.setSelected(true);
        setVisibleStartButton(true);
        hideSkipButton();
    }

    private void initGameSettings() {
        ObservableList<Player> whitePlayers = FXCollections.observableArrayList(
                playerFactory.getPlayer("humanPlayer", Disk.WHITE),
                playerFactory.getPlayer("aiLuckyPlayer", Disk.WHITE),
                playerFactory.getPlayer("aiCleverPlayer", Disk.WHITE)
        );
        ObservableList<Player> blackPlayers = FXCollections.observableArrayList(
                playerFactory.getPlayer("humanPlayer", Disk.BLACK),
                playerFactory.getPlayer("aiLuckyPlayer", Disk.BLACK),
                playerFactory.getPlayer("aiCleverPlayer", Disk.BLACK)
        );
        whitePlayerSelector.setItems(whitePlayers);
        blackPlayerSelector.setItems(blackPlayers);
        whitePlayerSelector.setValue(whitePlayers.get(0));
        blackPlayerSelector.setValue(blackPlayers.get(0));
    }

    private void applyGameSettings() {
        gameController.setWhitePlayer(whitePlayerSelector.getValue());
        gameController.setBlackPlayer(blackPlayerSelector.getValue());
    }

    private void initEventHandler() {
        gameFieldCanvas.addEventHandler(EventType.ROOT, gameController.getWhitePlayer().getGameFieldEventHandler());
        gameFieldCanvas.addEventHandler(EventType.ROOT, gameController.getBlackPlayer().getGameFieldEventHandler());
        skipButton.addEventHandler(EventType.ROOT, gameController.getWhitePlayer().getSkipButtonEventHandler());
        skipButton.addEventHandler(EventType.ROOT, gameController.getBlackPlayer().getSkipButtonEventHandler());
    }

    private void initScoreLabels() {
        whiteScore.setText("0");
        blackScore.setText("0");
        drawDisk(whiteScoreDisk.getGraphicsContext2D(), Disk.WHITE,
                0, 0, whiteScoreDisk.getWidth(), whiteScoreDisk.getHeight());
        drawDisk(blackScoreDisk.getGraphicsContext2D(), Disk.BLACK,
                0, 0, blackScoreDisk.getWidth(), blackScoreDisk.getHeight());
    }

    private void initGameField() {
        gameFieldCanvas.setWidth(model.getCanvasWidth());
        gameFieldCanvas.setHeight(model.getCanvasHeight());
    }

    private void resetGameSettings() {
        whitePlayerSelector.setValue(gameController.getWhitePlayer());
        blackPlayerSelector.setValue(gameController.getBlackPlayer());
    }

    @FXML
    private void startButtonAction() {
        applyGameSettings();
        setVisibleStartButton(false);
        showHideToggleButton.setVisible(false);
        gameFieldCanvas.setVisible(true);
        pack();
        gameController.startGame();
    }

    @FXML
    private void showHideToggleButtonAction() {
        if (showHideToggleButton.isSelected()) {
            gameFieldCanvas.setVisible(true);
        } else {
            gameFieldCanvas.setVisible(false);
        }
        pack();
    }

    @FXML
    private void applyButtonAction() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Apply settings");
        alert.setHeaderText("Game will be restarted");
        alert.setContentText("Do you want to continue?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            gameController.stopGame();
            applyGameSettings();
            gameController.startGame();
        } else {
            resetGameSettings();
            alert.close();
        }
    }

    @FXML
    private void restartButtonAction() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Restart");
        alert.setHeaderText("Game will be restarted");
        alert.setContentText("Do you want to continue?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            gameController.startGame();
        } else {
            alert.close();
        }
    }

    public void showWinner(Disk winner) {
        if (winner == Disk.WHITE) {
            whiteScore.setText("W");
            blackScore.setText("L");
        } else if (winner == Disk.BLACK) {
            whiteScore.setText("L");
            blackScore.setText("W");
        } else if (winner == null) {
            whiteScore.setText("D");
            blackScore.setText("D");
        }
        setVisibleStartButton(true);
        showHideToggleButton.setVisible(true);
        pack();
    }

    public void selectCurrentPlayer() {
        Disk disk = gameController.getCurrentPlayer().getDisk();
        if (disk == Disk.WHITE) {
            selectScoreDisk(whiteScoreDisk, Disk.WHITE);
            deselectScoreDisk(blackScoreDisk, Disk.BLACK);
        } else if (disk == Disk.BLACK) {
            selectScoreDisk(blackScoreDisk, Disk.BLACK);
            deselectScoreDisk(whiteScoreDisk, Disk.WHITE);
        }
    }

    public void updateGameScore() {
        whiteScore.setText(String.valueOf(gameController.getGameField().getWhiteDisksAmount()));
        blackScore.setText(String.valueOf(gameController.getGameField().getBlackDisksAmount()));
    }

    public void updateGameField() {
        drawGameField();
        drawGameFieldDisks();
    }

    public void putDiskToCell(Disk disk, int x, int y) {
        drawDiskInCell(disk, x, y);
    }

    private void drawGameField() {
        GraphicsContext gc = gameFieldCanvas.getGraphicsContext2D();
        gc.setFill(model.getBackgroundColor());
        gc.setStroke(model.getLineColor());
        gc.fillRect(0, 0, gameFieldCanvas.getWidth(), gameFieldCanvas.getHeight());
        gc.setLineWidth(model.getLineWidth());
        for (int i = model.getCellSize(); i < gameFieldCanvas.getWidth(); i += model.getCellSize()) {
            gc.strokeLine(i, 0, i, gameFieldCanvas.getHeight());
        }
        for (int j = model.getCellSize(); j < gameFieldCanvas.getHeight(); j += model.getCellSize()) {
            gc.strokeLine(0, j, gameFieldCanvas.getWidth(), j);
        }
    }

    private void drawGameFieldDisks() {
        for (Cell cell : gameController.getGameField()) {
            if (cell.isEmpty()) continue;
            drawDiskInCell(cell.getDisk(), cell.getX(), cell.getY());
        }
    }

    private void drawDiskInCell(Disk disk, int x, int y) {
        int cellPadding = (model.getCellSize() - model.getDiskSize()) / 2;
        drawDisk(gameFieldCanvas.getGraphicsContext2D(), disk,
                x * model.getCellSize() + cellPadding,
                y * model.getCellSize() + cellPadding,
                model.getDiskSize(), model.getDiskSize());
    }

    private void drawDisk(GraphicsContext gc, Disk disk, double x, double y,
                          double width, double height) {
        gc.setFill(model.getDiskColor(disk));
        gc.fillOval(x, y, width, height);
    }

    private void drawDiskBorder(GraphicsContext gc, double x, double y,
                                double width, double height, int borderWidth, Color borderColor) {
        gc.setStroke(borderColor);
        gc.setLineWidth(borderWidth);
        gc.strokeOval(x + borderWidth / 2.0, y + borderWidth / 2.0,
                width - borderWidth, height - borderWidth);
    }

    private void selectScoreDisk(Canvas scoreDisk, Disk disk) {
        GraphicsContext gc = scoreDisk.getGraphicsContext2D();
        gc.clearRect(0, 0, scoreDisk.getWidth(), scoreDisk.getHeight());
        drawDisk(gc, disk, 0, 0, scoreDisk.getWidth(), scoreDisk.getHeight());
        drawDiskBorder(gc, 0, 0, scoreDisk.getWidth(), scoreDisk.getHeight(),
                model.getSelectedBorderWidth(), model.getSelectedBorderColor());
    }

    private void deselectScoreDisk(Canvas scoreDisk, Disk disk) {
        GraphicsContext gc = scoreDisk.getGraphicsContext2D();
        gc.clearRect(0, 0, scoreDisk.getWidth(), scoreDisk.getHeight());
        drawDisk(gc, disk, 0, 0, scoreDisk.getWidth(), scoreDisk.getHeight());
    }

    private void setVisibleStartButton(boolean visible) {
        startButton.setVisible(visible);
        applyButton.setDisable(visible);
        restartButton.setDisable(visible);
    }

    public void hideSkipButton() {
        skipButton.setVisible(false);
    }

    public void showSkipButton() {
        skipButton.setVisible(true);
    }

    private void pack() {
        gameFieldCanvas.getScene().getWindow().sizeToScene();
    }
}