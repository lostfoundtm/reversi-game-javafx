package com.lostfound.reversi.controller;

import ch.qos.logback.classic.Logger;
import com.lostfound.reversi.model.Cell;
import com.lostfound.reversi.model.Disk;
import com.lostfound.reversi.model.GameField;
import com.lostfound.reversi.player.Player;
import com.lostfound.reversi.ui.MainController;
import com.lostfound.reversi.util.GameContainer;
import javafx.application.Platform;
import javafx.concurrent.Task;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicBoolean;

@Component
public class GameController {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(GameController.class);

    @Autowired
    private MainController mainController;
    @Autowired
    private GameContainer gameContainer;

    private Player buffer;

    private Player whitePlayer;
    private Player blackPlayer;

    private GameField gameField;
    private Player currentPlayer;
    private Player otherPlayer;

    private Task<Void> gameTask;

    private boolean running;

    public boolean isGameRunning() {
        return running;
    }

    public void startGame() {
        stopGame();
        gameField = createGameField();
        mainController.updateGameField();
        mainController.updateGameScore();
        currentPlayer = whitePlayer;
        otherPlayer = blackPlayer;
        mainController.selectCurrentPlayer();
        gameTask = createGameTask();
        Thread gameThread = new Thread(gameTask);
        gameThread.setDaemon(true);
        gameThread.start();
    }

    public void stopGame() {
        if (gameTask != null && gameTask.isRunning()) {
            gameTask.cancel();
        }
    }

    public void setWhitePlayer(Player whitePlayer) {
        this.whitePlayer = whitePlayer;
    }

    public void setBlackPlayer(Player blackPlayer) {
        this.blackPlayer = blackPlayer;
    }

    public Player getWhitePlayer() {
        return whitePlayer;
    }

    public Player getBlackPlayer() {
        return blackPlayer;
    }

    public GameField getGameField() {
        return gameField;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getOtherPlayer() {
        return otherPlayer;
    }

    private void nextPlayer() {
        buffer = currentPlayer;
        currentPlayer = otherPlayer;
        otherPlayer = buffer;
    }

    private GameField createGameField() {
        gameContainer.setCurrentGameField(gameContainer.getNewGameField());
        return gameContainer.getCurrentGameField();
    }

    protected Task<Void> createGameTask() {
        return new Task<Void>() {

            private static final long CHECK_DELAY = 10L;
            private AtomicBoolean executing = new AtomicBoolean(false);

            @Override
            protected Void call() throws Exception {
                executing.set(false);
                running = true;
                while (isGameRunning()) {
                    if (gameField.isEndOfGame()) break;
                    final Disk disk = currentPlayer.getDisk();
                    if (!gameField.hasPlayerMove(disk)) {
                        executeAndWait(() -> mainController.showSkipButton());
                        currentPlayer.skip();
                        executeAndWait(() -> mainController.hideSkipButton());
                        nextPlayer();
                        continue;
                    }
                    final Cell target = currentPlayer.move();
                    if (gameField.tryToApplyMove(disk, target)) {
                        executeAndWait(() -> {
                            mainController.updateGameField();
                            mainController.updateGameScore();
                        });
                        nextPlayer();
                        executeAndWait(() -> mainController.selectCurrentPlayer());
                    }
                }
                if (gameField.getGameWinner() == Disk.WHITE) {
                    whitePlayer.win();
                    blackPlayer.lose();
                } else if (gameField.getGameWinner() == Disk.BLACK) {
                    whitePlayer.lose();
                    blackPlayer.win();
                } else {
                    whitePlayer.draw();
                    blackPlayer.draw();
                }
                return null;
            }

            private void executeAndWait(Runnable runnable) throws InterruptedException {
                execute(runnable);
                waitForExecute();
            }

            private void execute(Runnable runnable) {
                executing.set(true);
                Platform.runLater(() -> {
                            runnable.run();
                            executing.set(false);
                        }
                );
            }

            private void waitForExecute() throws InterruptedException {
                while (executing.get()) {
                    Thread.sleep(CHECK_DELAY);
                }
            }

            @Override
            protected void succeeded() {
                running = false;
                execute(() -> mainController.showWinner(gameField.getGameWinner()));
                logger.info("Game succeeded.");
            }

            @Override
            protected void cancelled() {
                running = false;
                whitePlayer.stop();
                blackPlayer.stop();
                logger.info("Game canceled.");
            }

            @Override
            protected void failed() {
                running = false;
                whitePlayer.stop();
                blackPlayer.stop();
                logger.error("Fail during the game running!");
            }
        };
    }

}
