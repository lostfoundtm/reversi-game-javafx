package com.lostfound.reversi.player;

import com.lostfound.reversi.model.Cell;
import com.lostfound.reversi.model.Disk;
import javafx.event.Event;
import javafx.event.EventHandler;

public interface Player {

    boolean isMoving();

    void stop();

    Disk getDisk();

    Player setDisk(Disk disk);

    Cell move();

    void skip();

    default EventHandler<? super Event> getGameFieldEventHandler() {
        return null;
    }

    default EventHandler<? super Event> getSkipButtonEventHandler() {
        return null;
    }

    default void win() {
    }

    default void lose() {
    }

    default void draw() {
    }

}
