package com.lostfound.reversi.player.human;

import com.lostfound.reversi.model.Cell;
import com.lostfound.reversi.model.Disk;
import com.lostfound.reversi.player.Player;
import com.lostfound.reversi.util.GameUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("humanPlayer")
@Scope("prototype")
public class HumanPlayer implements Player {

    private static final Logger logger = LoggerFactory.getLogger(HumanPlayer.class);

    private static final long CHECK_DELAY = 120L;

    @Autowired
    private GameUtils gameUtils;

    private Disk disk;
    private boolean moving;
    private boolean waiting;
    private Cell target;

    @Override
    public boolean isMoving() {
        return moving;
    }

    protected boolean isWaiting() {
        return waiting;
    }

    @Override
    public Disk getDisk() {
        return disk;
    }

    @Override
    public Player setDisk(Disk disk) {
        this.disk = disk;
        return this;
    }

    @Override
    public Cell move() {
        moving = true;
        while (isMoving()) {
            try {
                Thread.sleep(CHECK_DELAY);
            } catch (InterruptedException e) {
                logger.error("Fail during " + this.toString() + ":[" + disk + "] player move: " + e.getMessage());
            }
        }
        return target;
    }

    @Override
    public void skip() {
        waiting = true;
        while (isWaiting()) {
            try {
                Thread.sleep(CHECK_DELAY);
            } catch (InterruptedException e) {
                logger.error("Fail during " + this.toString() + ":[" + disk + "] player skip move: " + e.getMessage());
            }
        }
    }

    @Override
    public void stop() {
        moving = false;
    }

    @Override
    public String toString() {
        return "Human";
    }

    @Override
    public EventHandler<Event> getGameFieldEventHandler() {
        return event -> {
            if (event.getEventType() == MouseEvent.MOUSE_RELEASED) {
                MouseEvent me = (MouseEvent) event;
                if (me.getButton() == MouseButton.PRIMARY) {
                    target = gameUtils.getCell(me.getX(), me.getY());
                    moving = false;
                }
            }
        };
    }

    @Override
    public EventHandler<? super Event> getSkipButtonEventHandler() {
        return event -> {
            if (event.getEventType() == ActionEvent.ACTION) {
                waiting = false;
            }
        };
    }
}
