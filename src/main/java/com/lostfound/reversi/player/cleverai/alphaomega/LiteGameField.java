package com.lostfound.reversi.player.cleverai.alphaomega;

public class LiteGameField {

    public static final byte WHITE_DISK = 0;
    public static final byte BLACK_DISK = 1;
    public static final byte EMPTY_CELL = -1;

    private final byte[][] fieldMatrix;
    private final int[] numberOfDisks;
    private int width;
    private int height;

    protected LiteGameField(int width, int height) {
        this.width = width;
        this.height = height;
        fieldMatrix = new byte[width][height];
        numberOfDisks = new int[2];
        clear();
    }

    protected void clear() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                fieldMatrix[x][y] = EMPTY_CELL;
            }
        }
        resetDiskCounter();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getNumberOf(byte diskType) {
        return numberOfDisks[diskType];
    }

    public byte reverseDiskType(byte diskType) {
        return (byte) (diskType ^ 1);
    }

    public void reverseDiskIn(int x, int y) {
        numberOfDisks[fieldMatrix[x][y]]--;
        fieldMatrix[x][y] ^= 1;
        numberOfDisks[fieldMatrix[x][y]]++;
    }

    public boolean isEmptyCell(int x, int y) {
        return fieldMatrix[x][y] == EMPTY_CELL;
    }

    public byte peekDiskIn(int x, int y) {
        return fieldMatrix[x][y];
    }

    protected void putDiskTo(int x, int y, byte diskType) {
        fieldMatrix[x][y] = diskType;
        numberOfDisks[diskType]++;
    }

    protected byte removeDiskFrom(int x, int y) {
        byte removedDisk = fieldMatrix[x][y];
        fieldMatrix[x][y] = EMPTY_CELL;
        if (removedDisk != EMPTY_CELL) {
            numberOfDisks[removedDisk]--;
        }
        return removedDisk;
    }

    protected byte[][] createSnapshot() {
        byte[][] snapshot = new byte[width][height];
        for (int x = 0; x < width; x++) {
            System.arraycopy(fieldMatrix[x], 0, snapshot[x], 0, height);
        }
        return snapshot;
    }

    protected LiteGameField applySnapshot(byte[][] snapshot) {
        for (int x = 0; x < width; x++) {
            System.arraycopy(snapshot[x], 0, fieldMatrix[x], 0, height);
        }
        recountDisks();
        return this;
    }

    protected void resetDiskCounter() {
        numberOfDisks[WHITE_DISK] = 0;
        numberOfDisks[BLACK_DISK] = 0;
    }

    protected void recountDisks() {
        resetDiskCounter();
        forEachDisk(diskType -> {
            if (diskType != EMPTY_CELL) {
                numberOfDisks[diskType]++;
            }
        });
    }

    protected void forEachCell(LiteCellAction action) {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                action.apply(x, y);
            }
        }
    }

    protected void forEachDisk(LiteDiskAction action) {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                action.apply(fieldMatrix[x][y]);
            }
        }
    }

    @Override
    public LiteGameField clone() {
        final LiteGameField liteGameFieldClone = new LiteGameField(width, height);
        liteGameFieldClone.applySnapshot(fieldMatrix);
        return liteGameFieldClone;
    }


    @Override
    public String toString() {
        String s = "";
        s += "White disks: " + numberOfDisks[WHITE_DISK];
        s += System.lineSeparator();
        s += "Black disks: " + numberOfDisks[BLACK_DISK];
        s += System.lineSeparator();
        s += "  ";
        for (int x = 0; x < width; x++) {
            s += " " + x;
        }
        for (int y = 0; y < height; y++) {
            s += System.lineSeparator();
            s += y + " ";
            for (int x = 0; x < width; x++) {
                byte diskType = fieldMatrix[x][y];
                s += " " + (diskType == EMPTY_CELL ? "_" : diskType == WHITE_DISK ? "W" : "B");
            }
        }
        return s;
    }
}
