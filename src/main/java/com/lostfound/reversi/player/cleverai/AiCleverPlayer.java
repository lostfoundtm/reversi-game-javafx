package com.lostfound.reversi.player.cleverai;

import com.lostfound.reversi.model.*;
import com.lostfound.reversi.player.Player;
import com.lostfound.reversi.player.cleverai.model.Rate;
import com.lostfound.reversi.service.MoveSaveService;
import com.lostfound.reversi.util.GameContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static com.lostfound.reversi.util.CommonUtils.getCycled;
import static java.util.Collections.max;

@Component("aiCleverPlayer")
@Scope("prototype")
@PropertySource("classpath:clever-ai.properties")
public class AiCleverPlayer implements Player {

    private static final Logger logger = LoggerFactory.getLogger(AiCleverPlayer.class);

    @Value("${ai.delay}")
    private long DELAY;

    @Value("${enable_study}")
    private boolean enabledStudy;
    @Value("${enable_extended_study}")
    private boolean enabledExtendedStudy;
    @Value("${lower_win_rate}")
    private double lowerWinRate;
    @Value("${lower_extended_win_rate}")
    private double lowerExtendedWinRate;

    @Value("${win_weight}")
    private double winWeight;
    @Value("${draw_weight}")
    private double drawWeight;

    @Value("${analysis_deep}")
    private int ANALYSIS_DEEP;
    @Value("${secondary_analysis_deep}")
    private int SECONDARY_ANALYSIS_DEEP;

    @Autowired
    private Rate rate;

    @Autowired
    private GameContainer gameContainer;
    @Autowired
    private MoveSaveService moveSaveService;

    @Value("${threads_amount}")
    private int threadsAmount;
    private ExecutorService executorService;
    private Random random;

    private List<Move> moves;
    private Disk disk;
    private boolean moving;

    @PostConstruct
    private void init() {
        moves = new ArrayList<>();
        random = new Random();
        executorService = Executors.newFixedThreadPool(threadsAmount);
    }

    @Override
    public boolean isMoving() {
        return moving;
    }

    @Override
    public Disk getDisk() {
        return disk;
    }

    @Override
    public Player setDisk(Disk disk) {
        this.disk = disk;
        return this;
    }

    @Override
    public Cell move() {
        moving = true;
        GameField gf = gameContainer.getCurrentGameField();

        Cell target = cleverMove(gf);

        try {
            Thread.sleep(DELAY);
        } catch (InterruptedException e) {
            logger.error("Fail during " + this.toString() + ":[" + disk + "] player move: " + e.getMessage());
        }

        if (enabledStudy) {
            moves.add(new Move(gf.hashCode(), gf.getWhiteDisksAmount(), gf.getBlackDisksAmount(), target));
        }

        moving = false;
        return target;
    }

    private Cell cleverMove(GameField gf) {
        Cell cell = null;
        boolean isMoveSelected = false;
        List<Target> savedTargets = getSavedTargets(gf);
        List<Cell> availableMoves = gf.getAvailableMoves(getDisk());
        if (savedTargets != null && !savedTargets.isEmpty()) {
            List<Cell> savedMoves = savedTargets.stream().map(Target::getCell).collect(Collectors.toList());
            Target target = max(savedTargets, (t1, t2) -> Double.compare(getWinRate(t1), getWinRate(t2)));
            Double winRate = getWinRate(target);
            if (enabledExtendedStudy && savedTargets.size() < availableMoves.size() && winRate < lowerExtendedWinRate) {
                logger.info("[Extended study] Don't remember high rated moves. Try to learn other moves");
                availableMoves.removeAll(savedMoves);
                cell = deepMove(gf, availableMoves);
                isMoveSelected = true;
            } else if (winRate >= lowerWinRate) {
                logger.info("Make remembered move [" + target.getCell() + "], high win rate - " + winRate * 100 + "%");
                cell = target.getCell();
                isMoveSelected = true;
            } else {
                logger.info("Don't make remembered move [" + target.getCell() + "], lowe win rate - " + winRate * 100 + "%");
            }
        }
        if (!isMoveSelected) {
            logger.info("Make move by deep analysis");
            cell = deepMove(gf, availableMoves);
        }
        return cell;
    }

    private Double getWinRate(Target t) {
        return (t.getWins() * winWeight + t.getDraws() * drawWeight) / t.getAmount();
    }

    private Cell deepMove(GameField gameField, List<Cell> availableMoves) {
        Map<Cell, Integer> moveMap = new HashMap<>();
        availableMoves.forEach(move -> moveMap.put(move, 0));

        Map<Cell, Callable<Integer>> taskMap = new HashMap<>(availableMoves.size());
        moveMap.forEach((move, rate) -> {
            Callable<Integer> task = () -> deepAnalysis(move, gameField, getDisk(),
                    0, ANALYSIS_DEEP, SECONDARY_ANALYSIS_DEEP != -1);
            taskMap.put(move, task);
        });
        taskMap.forEach((move, task) -> executorService.submit(task));

        taskMap.forEach((move, task) -> {
            try {
                moveMap.put(move, task.call());
            } catch (Exception e) {
                logger.error("Error during deep analysis execution. " + e.getMessage());
            }
        });

        int maxRate = Collections.max(moveMap.entrySet(), (o1, o2) -> o1.getValue().compareTo(o2.getValue())).getValue();
        List<Cell> maxRatedMoves = new ArrayList<>();
        moveMap.forEach((move, rate) -> {
            if (rate == maxRate) maxRatedMoves.add(move);
        });

        return maxRatedMoves.get(random.nextInt(maxRatedMoves.size()));
    }

    private int deepAnalysis(Cell moveTo, GameField gameField, Disk currentDisk, int deep, int maxDeep, boolean secondary) {
        // apply move
        GameField currentGameField = gameContainer.getNewGameField();
        currentGameField.cloneState(gameField);
        currentGameField.tryToApplyMove(currentDisk, moveTo);

        // next available moves
        Disk nextPlayerDisk = currentDisk.reverse();
        List<Cell> availableMoves = currentGameField.getAvailableMoves(nextPlayerDisk);

        if (maxDeep != -1 && deep >= maxDeep || availableMoves.isEmpty()) {
            return calculateMoveRate(currentGameField, getDisk());
        } else {
            Cell bestMove;
            if (secondary) {
                Map<Cell, Integer> moveMap = availableMoves.stream().collect(
                        Collectors.toMap(move -> move, move -> deepAnalysis(move, currentGameField, nextPlayerDisk,
                                deep, deep + SECONDARY_ANALYSIS_DEEP, false)));
                bestMove = Collections.max(moveMap.entrySet(), (o1, o2) -> o1.getValue().compareTo(o2.getValue())).getKey();
            } else {
                bestMove = Collections.max(availableMoves, (o1, o2) ->
                        Double.compare(calculateMoveRate(currentGameField, o1, nextPlayerDisk),
                                calculateMoveRate(currentGameField, o2, nextPlayerDisk)));
            }
            return deepAnalysis(bestMove, currentGameField, nextPlayerDisk, deep + 1, maxDeep, secondary);
        }
    }

    private int calculateMoveRate(GameField gf, Cell moveTo, Disk playerDisk) {
        GameField ngf = gameContainer.getNewGameField();
        ngf.cloneState(gf);
        ngf.tryToApplyMove(playerDisk, moveTo);
        return calculateMoveRate(ngf, playerDisk);
    }

    private int calculateMoveRate(GameField gf, Disk playerDisk) {
        final Disk enemyDisk = playerDisk.reverse();
        int moveRate = 0;

        for (Cell cell : gf) {
            if (cell.isEmpty()) continue;

            if (cell.getDisk() == playerDisk) {
                if (isCornerCell(gf, cell)) {
                    moveRate += rate.forPlayer(playerDisk, playerDisk).get(Rate.CORNER_DISK_RATE);
                } else if (isDeadCell(gf, cell)) {
                    moveRate += rate.forPlayer(playerDisk, playerDisk).get(Rate.DEAD_DISK_RATE);
                } else if (isBorderedCell(gf, cell)) {
                    moveRate += rate.forPlayer(playerDisk, playerDisk).get(Rate.BORDERED_DISK_RATE);
                } else {
                    moveRate += rate.forPlayer(playerDisk, playerDisk).get(Rate.DISK_RATE);
                }
            } else {
                if (isCornerCell(gf, cell)) {
                    moveRate -= rate.forPlayer(playerDisk, enemyDisk).get(Rate.CORNER_DISK_RATE);
                } else if (isDeadCell(gf, cell)) {
                    moveRate -= rate.forPlayer(playerDisk, enemyDisk).get(Rate.DEAD_DISK_RATE);
                } else if (isBorderedCell(gf, cell)) {
                    moveRate -= rate.forPlayer(playerDisk, enemyDisk).get(Rate.BORDERED_DISK_RATE);
                } else {
                    moveRate -= rate.forPlayer(playerDisk, enemyDisk).get(Rate.DISK_RATE);
                }
            }
        }
        return moveRate;
    }

    private boolean isCornerCell(GameField gf, Cell cell) {
        return (cell.getX() == 0 && cell.getY() == 0) ||
                (cell.getX() == gf.getWidth() - 1 && cell.getY() == 0) ||
                (cell.getX() == 0 && cell.getY() == gf.getHeight() - 1) ||
                (cell.getX() == gf.getWidth() - 1 && cell.getY() == gf.getHeight() - 1);
    }

    private boolean isBorderedCell(GameField gf, Cell cell) {
        return (cell.getX() == 0 && cell.getY() == 0) ||
                (cell.getX() == gf.getWidth() - 1 && cell.getY() == gf.getHeight() - 1) ||
                (cell.getX() == gf.getWidth() - 1 && cell.getY() == 0) ||
                (cell.getX() == 0 && cell.getY() == gf.getHeight() - 1);
    }

    private boolean isDeadCell(GameField gf, Cell cell) {
        return isDeadCell(gf, cell, new HashSet<>(), new HashMap<>());
    }

    private boolean isDeadCell(GameField gf, Cell cell, Set<Cell> checking, Map<Cell, Boolean> checked) {
        final int[] dx = {0, 1, 1, 1, 0, -1, -1, -1};
        final int[] dy = {-1, -1, 0, 1, 1, 1, 0, -1};
        final Boolean[] deadAround = new Boolean[8];

        Map<Integer, Cell> dependent = new HashMap<>();

        for (int i = 0; i < deadAround.length && canBeDead(deadAround); i++) {
            Cell roundCell = Cell.create(cell, dx[i], dy[i]);
            if (checked.containsKey(roundCell)) {
                deadAround[i] = checked.get(roundCell);
            } else if (!gf.isCellOnGameField(roundCell)) {
                deadAround[i] = true;
            } else if (checking.contains(roundCell)) {
                deadAround[i] = false;
            } else if (gf.get(roundCell) == cell.getDisk()) {
                dependent.put(i, roundCell);
            } else if (gf.get(roundCell) != cell.getDisk()) {
                deadAround[i] = false;
            }
        }
        if (!isDead(deadAround) && canBeDead(deadAround) && !dependent.isEmpty()) {
            checking.add(cell);
            dependent.entrySet().forEach(entry ->
                    deadAround[entry.getKey()] = isDeadCell(gf, entry.getValue(), checking, checked));
        }
        checking.remove(cell);
        checked.put(cell, isDead(deadAround));
        return checked.get(cell);
    }

    private boolean isDead(Boolean[] deadAround) {
        int deadCounter = 0;
        for (int i = -deadAround.length / 2; i < deadAround.length; i++) {
            Boolean dead = getCycled(deadAround, i);
            if (dead != null && dead) {
                deadCounter++;
                if (deadCounter >= 4) break;
            } else {
                deadCounter = 0;
            }
        }
        return deadCounter >= 4;
    }

    private boolean canBeDead(Boolean[] deadAround) {
        int deadCounter = 0;
        for (int i = -deadAround.length / 2; i < deadAround.length; i++) {
            Boolean dead = getCycled(deadAround, i);
            if (dead == null || dead) {
                deadCounter++;
                if (deadCounter >= 4) break;
            } else {
                deadCounter = 0;
            }
        }
        return deadCounter >= 4;
    }

    @Override
    public void win() {
        study(1);
    }

    @Override
    public void lose() {
        study(-1);
    }

    @Override
    public void draw() {
        study(0);
    }

    private void study(int result) {
        if (enabledStudy) {
            saveGame(result);
        }
    }

    private void saveGame(int result) {
        GameField gf = gameContainer.getCurrentGameField();
        moveSaveService.saveGame("clever_ai", getDisk(), gf.getWidth(), gf.getHeight(), moves, result);
        moves.clear();
    }

    private List<Target> getSavedTargets(GameField gf) {
        return moveSaveService.get("clever_ai", getDisk(), gf.getWidth(), gf.getHeight(),
                gf.getWhiteDisksAmount(), gf.getBlackDisksAmount(), gf.hashCode());
    }

    @Override
    public void skip() {
    }

    @Override
    public void stop() {
        moving = false;
        executorService.shutdown();
    }

    @Override
    public String toString() {
        return "Clever AI";
    }
}
