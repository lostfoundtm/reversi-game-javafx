package com.lostfound.reversi.player.cleverai.alphaomega.processor;

import java.util.List;

public class LiteNode {

    private int value;
    private LiteNode parent;
    private List<LiteNode> children;

    private int level;
    private byte[][] snapshot;


    public LiteNode(LiteNode parent, byte[][] snapshot) {
        this.parent = parent;
        level = parent.getLevel() + 1;
    }

    public int getLevel() {
        return level;
    }

    public byte[][] getSnapshot() {
        return snapshot;
    }

    public LiteNode getParent() {
        return parent;
    }

    public List<LiteNode> getChildren() {
        return children;
    }
}
