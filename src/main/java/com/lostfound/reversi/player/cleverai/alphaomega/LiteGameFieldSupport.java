package com.lostfound.reversi.player.cleverai.alphaomega;

import java.util.ArrayList;
import java.util.List;

public class LiteGameFieldSupport {

    // all directions to go
    private static final int[] deltaX = {0, 1, 1, 1, 0, -1, -1, -1};
    private static final int[] deltaY = {-1, -1, 0, 1, 1, 1, 0, -1};

    public void applyMove(LiteGameField liteGameField, int x, int y, byte playerDiskType) {
        liteGameField.putDiskTo(x, y, playerDiskType);
        moveIfValid(liteGameField, x, y, playerDiskType, liteGameField::reverseDiskIn);
    }

    public boolean isValidCell(LiteGameField liteGameField, int x, int y) {
        return x > -1 && y > -1 && x < liteGameField.getWidth() && y < liteGameField.getHeight();
    }

    public List<LiteCell> getAvailableMoves(LiteGameField liteGameField, byte playerDiskType) {
        final List<LiteCell> availableMoves = new ArrayList<>();
        liteGameField.forEachCell((x, y) -> {
            if (liteGameField.isEmptyCell(x, y) && moveIfValid(liteGameField, x, y, playerDiskType, null)) {
                availableMoves.add(new LiteCell(x, y));
            }
        });
        return availableMoves;
    }

    public boolean moveIfValid(LiteGameField liteGameField, int x, int y, byte playerDiskType,
                               LiteCellAction hitAction) {
        final byte enemyDiskType = liteGameField.reverseDiskType(playerDiskType);
        boolean result = false;
        boolean hit = false;
        for (int i = 0; i < deltaX.length; i++) {
            int dx = x + deltaX[i];
            int dy = y + deltaY[i];
            boolean enemyDiskFound = false;
            boolean playerDiskFound = false;
            while (isValidCell(liteGameField, dx, dy)) {
                byte currentDisk = liteGameField.peekDiskIn(dx, dy);
                if (currentDisk == enemyDiskType) {
                    if (!enemyDiskFound) enemyDiskFound = true;
                    if (hit) hitAction.apply(dx, dy);
                } else if (enemyDiskFound && currentDisk == playerDiskType) {
                    playerDiskFound = true;
                    break;
                } else if (currentDisk == LiteGameField.EMPTY_CELL) {
                    break;
                }
                dx += deltaX[i];
                dy += deltaY[i];
            }
            result = enemyDiskFound && playerDiskFound;
            if (result) {
                if (hitAction == null) {
                    break;
                } else if (!hit) {
                    hit = true;
                    i--;
                } else {
                    hit = false;
                }
            }
        }
        return result;
    }

}
