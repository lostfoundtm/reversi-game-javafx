package com.lostfound.reversi.player.cleverai.alphaomega;

import com.lostfound.reversi.model.Cell;
import com.lostfound.reversi.model.Disk;
import com.lostfound.reversi.model.GameField;

public class LiteConverter {
    public LiteGameField getLiteGameField(GameField gameField) {
        final LiteGameField liteGameField = new LiteGameField(gameField.getWidth(), gameField.getHeight());
        for (Cell cell : gameField) {
            if (!cell.isEmpty()) {
                liteGameField.putDiskTo(cell.getX(), cell.getY(), getLiteDiskType(cell.getDisk()));
            }
        }
        return liteGameField;
    }

    protected byte getLiteDiskType(Disk disk) {
        byte diskType;
        if (disk == Disk.WHITE) {
            diskType = LiteGameField.WHITE_DISK;
        } else if (disk == Disk.BLACK) {
            diskType = LiteGameField.BLACK_DISK;
        } else {
            diskType = LiteGameField.EMPTY_CELL;
        }
        return diskType;
    }

}
