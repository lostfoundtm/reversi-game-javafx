package com.lostfound.reversi.player.cleverai.alphaomega;

@FunctionalInterface
public interface LiteDiskAction {

    void apply(byte diskType);
}
