package com.lostfound.reversi.player.cleverai.alphaomega;

@FunctionalInterface
public interface LiteCellAction {

    void apply(int x, int y);
}
