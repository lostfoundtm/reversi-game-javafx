package com.lostfound.reversi.player.cleverai.alphaomega;

public class LiteCell {
    private int x;
    private int y;

    public LiteCell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return x + "-" + y;
    }
}
