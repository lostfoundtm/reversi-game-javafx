package com.lostfound.reversi.player.cleverai.model;

import com.lostfound.reversi.model.Disk;
import com.lostfound.reversi.model.GameFieldModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class Rate {

    public static final String WIN_RATE = "WIN_RATE";
    public static final String DRAW_RATE = "DRAW_RATE";
    public static final String LOSE_RATE = "LOSE_RATE";
    public static final String DEAD_DISK_RATE = "DEAD_DISK_RATE";
    public static final String CORNER_DISK_RATE = "CORNER_DISK_RATE";
    public static final String DISK_RATE = "DISK_RATE";
    public static final String BORDERED_DISK_RATE = "BORDERED_DISK_RATE";

    private Map<Boolean, Map<String, Integer>> rateMap;

    @Value("${rate_scale}")
    private int rateScale;

    @Value("#{${ai_rate_map}}")
    private Map<String, Integer> aiMap;
    @Value("#{${enemy_rate_map}}")
    private Map<String, Integer> enemyMap;
    @Autowired
    private GameFieldModel model;

    private Boolean player;

    @PostConstruct
    private void init() {
        rateScale = (model.getWidth() * model.getHeight() / 2) / rateScale;
        rateMap = new HashMap<>();
        rateMap.put(true, aiMap);
        rateMap.put(false, enemyMap);
    }

    public Rate forPlayer(Disk ai, Disk current) {
        player = ai == current;
        return this;
    }

    public int get(String key) {
        return rateMap.get(player).get(key) * rateScale;
    }

}
