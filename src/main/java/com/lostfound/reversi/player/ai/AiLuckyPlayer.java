package com.lostfound.reversi.player.ai;

import com.lostfound.reversi.model.Cell;
import com.lostfound.reversi.model.Disk;
import com.lostfound.reversi.player.Player;
import com.lostfound.reversi.util.GameContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Component("aiLuckyPlayer")
@Scope("prototype")
public class AiLuckyPlayer implements Player {

    private static final Logger logger = LoggerFactory.getLogger(AiLuckyPlayer.class);

    @Value("${ai.delay}")
    private long DELAY;

    @Autowired
    private GameContainer gameContainer;

    private Random random;

    private Disk disk;
    private boolean moving;

    public AiLuckyPlayer() {
        random = new Random();
    }

    @Override
    public boolean isMoving() {
        return moving;
    }

    @Override
    public Disk getDisk() {
        return disk;
    }

    @Override
    public Player setDisk(Disk disk) {
        this.disk = disk;
        return this;
    }

    @Override
    public Cell move() {
        moving = true;
        List<Cell> availableMoves = gameContainer.getCurrentGameField().getAvailableMoves(disk);
        Cell target = availableMoves.get(random.nextInt(availableMoves.size()));
        try {
            Thread.sleep(DELAY);
        } catch (InterruptedException e) {
            logger.error("Fail during " + this.toString() + ":[" + disk + "] player move: " + e.getMessage());
        }
        moving = false;
        return target;
    }

    @Override
    public void skip() {
    }

    @Override
    public void stop() {
        moving = false;
    }

    @Override
    public String toString() {
        return "Lucky AI";
    }
}
