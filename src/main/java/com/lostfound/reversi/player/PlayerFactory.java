package com.lostfound.reversi.player;

import com.lostfound.reversi.model.Disk;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

@Component
public class PlayerFactory {

    public Player getPlayer(String playerName, Disk disk) {
        switch (playerName) {
            case "humanPlayer":
                return getHumanPlayer().setDisk(disk);
            case "aiLuckyPlayer":
                return getAiLuckyPlayer().setDisk(disk);
            case "aiCleverPlayer":
                return getAiCleverPlayer().setDisk(disk);
            default:
                return null;
        }
    }

    @Lookup("humanPlayer")
    protected Player getHumanPlayer() {
        return null;
    }

    @Lookup("aiLuckyPlayer")
    protected Player getAiLuckyPlayer() {
        return null;
    }

    @Lookup("aiCleverPlayer")
    protected Player getAiCleverPlayer() {
        return null;
    }

}
