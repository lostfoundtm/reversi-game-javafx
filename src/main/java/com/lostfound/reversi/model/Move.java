package com.lostfound.reversi.model;

public class Move {
    private int hash;
    private int whiteDisks;
    private int blackDisks;
    private Target target;

    public Move(int hash, int whiteDisks, int blackDisks, Cell target) {
        this.hash = hash;
        this.target = new Target(target);
        this.whiteDisks = whiteDisks;
        this.blackDisks = blackDisks;
    }

    public int getHash() {
        return hash;
    }

    public Target getTarget() {
        return target;
    }

    public int getWhiteDisks() {
        return whiteDisks;
    }

    public int getBlackDisks() {
        return blackDisks;
    }
}