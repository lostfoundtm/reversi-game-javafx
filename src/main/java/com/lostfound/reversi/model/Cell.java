package com.lostfound.reversi.model;

import com.lostfound.reversi.exception.CellFormatException;

import java.io.Serializable;

public class Cell implements Serializable {
    private int x;
    private int y;

    private Disk disk;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Cell(int x, int y, Disk disk) {
        this.x = x;
        this.y = y;
        this.disk = disk;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Disk getDisk() {
        return disk;
    }

    public void setDisk(Disk disk) {
        this.disk = disk;
    }

    public Disk reverseDisk() {
        if (disk != null) {
            disk = disk.reverse();
        }
        return disk;
    }

    public boolean isEmpty() {
        return disk == null;
    }

    public static Cell parse(String sc) {
        String[] split = sc.split(":");
        if (split.length != 2) {
            throw new CellFormatException("Wrong cell format \"" + sc + "\"");
        }
        return new Cell(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
    }

    public static Cell create(int x, int y) {
        return new Cell(x, y);
    }

    public static Cell create(Cell cell, int dx, int dy) {
        return new Cell(cell.getX() + dx, cell.getY() + dy);
    }

    @Override
    public String toString() {
        return x + ":" + y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cell cell = (Cell) o;

        if (x != cell.x) return false;
        return y == cell.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
