package com.lostfound.reversi.model;

import javafx.scene.paint.Color;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class GameFieldModel {

    @Value("${reversi.gameField.width}")
    private int width;
    @Value("${reversi.gameField.height}")
    private int height;

    @Value("#{T(javafx.scene.paint.Color).web('${ui.gameField.background.color}')}")
    private Color backgroundColor;
    @Value("#{T(javafx.scene.paint.Color).web('${ui.gameField.line.color}')}")
    private Color lineColor;
    @Value("#{T(javafx.scene.paint.Color).web('${ui.gameField.text.color}')}")
    private Color textColor;
    @Value("#{T(javafx.scene.paint.Color).web('${ui.score.selected.disk.border.color}')}")
    private Color selectedBorderColor;

    @Value("${ui.score.selected.disk.border.width}")
    private int selectedBorderWidth;

    @Value("${ui.gameField.line.width}")
    private int lineWidth;

    @Value("${ui.gameField.cell.size}")
    private int cellSize;
    @Value("${ui.gameField.disk.size}")
    private int diskSize;

    @Value("#{{'WHITE' : T(javafx.scene.paint.Color).web('${ui.gameField.disk.white.color}')," +
            "'BLACK' : T(javafx.scene.paint.Color).web('${ui.gameField.disk.black.color}')}}")
    private Map<Disk, Color> diskColorMap;

    @Value("${reversi.gameField.start.position.white}")
    private String[] whiteStartPositions;
    @Value("${reversi.gameField.start.position.black}")
    private String[] blackStartPositions;

    private List<Cell> whiteStartCells;
    private List<Cell> blackStartCells;

    @PostConstruct
    private void init() {
        whiteStartCells = new ArrayList<>(whiteStartPositions.length);
        blackStartCells = new ArrayList<>(blackStartPositions.length);
        for (String position : whiteStartPositions) {
            whiteStartCells.add(Cell.parse(position));
        }
        for (String position : blackStartPositions) {
            blackStartCells.add(Cell.parse(position));
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public int getCellSize() {
        return cellSize;
    }

    public Color getLineColor() {
        return lineColor;
    }

    public Color getTextColor() {
        return textColor;
    }

    public double getCanvasWidth() {
        return width * cellSize;
    }

    public double getCanvasHeight() {
        return height * cellSize;
    }

    public int getLineWidth() {
        return lineWidth;
    }

    public int getDiskSize() {
        return diskSize;
    }

    public List<Cell> getWhiteStartCells() {
        return whiteStartCells;
    }

    public List<Cell> getBlackStartCells() {
        return blackStartCells;
    }

    public Color getDiskColor(Disk disk) {
        return diskColorMap.get(disk);
    }

    public Color getSelectedBorderColor() {
        return selectedBorderColor;
    }

    public int getSelectedBorderWidth() {
        return selectedBorderWidth;
    }
}
