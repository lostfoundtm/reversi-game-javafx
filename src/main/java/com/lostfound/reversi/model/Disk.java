package com.lostfound.reversi.model;

public enum Disk {
    WHITE, BLACK;

    public Disk reverse() {
        return this == WHITE ? BLACK : WHITE;
    }
}
