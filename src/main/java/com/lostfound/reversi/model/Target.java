package com.lostfound.reversi.model;

import java.io.Serializable;

public class Target implements Serializable {

    private Cell target;
    private int wins;
    private int loses;
    private int draws;

    public Target(Cell target) {
        this.target = target;
    }

    public void update(int result) {
        switch (result) {
            case 1:
                wins++;
                break;
            case -1:
                loses++;
                break;
            case 0:
                draws++;
                break;
        }
    }

    public int getAmount() {
        return wins + loses + draws;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLoses() {
        return loses;
    }

    public void setLoses(int loses) {
        this.loses = loses;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public Cell getCell() {
        return target;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Target target1 = (Target) o;

        return target != null ? target.equals(target1.target) : target1.target == null;

    }

    @Override
    public int hashCode() {
        return target != null ? target.hashCode() : 0;
    }
}
