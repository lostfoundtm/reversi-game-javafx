package com.lostfound.reversi.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Lazy
@Component("gameField")
@Scope("prototype")
public class GameField implements Iterable<Cell> {

    @Autowired
    private GameFieldModel model;

    private Map<Disk, AtomicInteger> diskCounter;

    private List<Cell> hitDisksCollector;
    private Cell[][] fieldMatrix;
    private int width;
    private int height;

    @PostConstruct
    private void init() {
        width = model.getWidth();
        height = model.getHeight();
        fieldMatrix = new Cell[width][height];
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                fieldMatrix[x][y] = new Cell(x, y);
            }
        }

        hitDisksCollector = new ArrayList<>();

        diskCounter = new EnumMap<>(Disk.class);
        diskCounter.put(Disk.WHITE, new AtomicInteger());
        diskCounter.put(Disk.BLACK, new AtomicInteger());

        model.getWhiteStartCells().forEach(cell -> setDiskToCell(Disk.WHITE, cell));
        model.getBlackStartCells().forEach(cell -> setDiskToCell(Disk.BLACK, cell));
    }

    public void cloneState(GameField gameField) {
        if (width != gameField.getWidth() || height != gameField.getHeight()) {
            throw new IllegalArgumentException("Game fields have different sizes");
        }
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                fieldMatrix[x][y].setDisk(gameField.get(x, y));
            }
        }
        diskCounter.get(Disk.WHITE).set(gameField.getWhiteDisksAmount());
        diskCounter.get(Disk.BLACK).set(gameField.getBlackDisksAmount());
    }

    public Disk get(Cell cell) {
        return get(cell.getX(), cell.getY());
    }

    public Disk get(int x, int y) {
        return fieldMatrix[x][y].getDisk();
    }

    public int getWhiteDisksAmount() {
        return diskCounter.get(Disk.WHITE).get();
    }

    public int getBlackDisksAmount() {
        return diskCounter.get(Disk.BLACK).get();
    }

    public int getDisksAmount(Disk disk) {
        return diskCounter.get(disk).get();
    }


    public void setDiskToCell(Disk disk, Cell cell) {
        fieldMatrix[cell.getX()][cell.getY()].setDisk(disk);
        diskCounter.get(disk).incrementAndGet();
    }

    public boolean tryToApplyMove(Disk disk, Cell target) {
        boolean isValidMove = false;
        hitDisksCollector.clear();
        if (isCellOnGameField(target) && isCellFree(target) && isValidMove(target, disk)) {
            setDiskToCell(disk, target);
            isValidMove = true;
            hitDisksCollector.forEach(cell -> {
                diskCounter.get(cell.getDisk()).decrementAndGet();
                diskCounter.get(cell.reverseDisk()).incrementAndGet();
            });
        }
        return isValidMove;
    }

    public List<Cell> getHitDisks() {
        return hitDisksCollector;
    }

    public boolean isCellFree(Cell cell) {
        return fieldMatrix[cell.getX()][cell.getY()].getDisk() == null;
    }

    public boolean isCellOnGameField(Cell cell) {
        return cell.getX() > -1 && cell.getY() > -1 &&
                cell.getX() < width && cell.getY() < height;
    }

    public boolean isValidMove(Cell cell, Disk disk) {
        // left || right || up || down
        return isValidMoveShift(disk, cell.getX(), cell.getY(), -1, -1, -1, 0) |
                isValidMoveShift(disk, cell.getX(), cell.getY(), width, height, 1, 0) |
                isValidMoveShift(disk, cell.getX(), cell.getY(), -1, -1, 0, -1) |
                isValidMoveShift(disk, cell.getX(), cell.getY(), width, height, 0, 1) |
                isValidMoveShift(disk, cell.getX(), cell.getY(), -1, -1, -1, -1) |
                isValidMoveShift(disk, cell.getX(), cell.getY(), -1, height, -1, 1) |
                isValidMoveShift(disk, cell.getX(), cell.getY(), width, -1, 1, -1) |
                isValidMoveShift(disk, cell.getX(), cell.getY(), width, height, 1, 1);
    }

    private boolean isValidMoveShift(Disk disk, int xStart, int yStart,
                                     int xEnd, int yEnd, int xShift, int yShift) {
        List<Cell> hitDisks = new ArrayList<>();
        boolean unionFound = false;
        boolean enemyFount = false;
        final Disk enemyDisk = disk.reverse();
        for (int x = xStart + xShift, y = yStart + yShift;
             x != xEnd && y != yEnd; x += xShift, y += yShift) {
            if (fieldMatrix[x][y].isEmpty()) {
                break;
            } else if (fieldMatrix[x][y].getDisk() == enemyDisk) {
                enemyFount = true;
                hitDisks.add(fieldMatrix[x][y]);
            } else if (fieldMatrix[x][y].getDisk() == disk) {
                unionFound = true;
                break;
            }
        }
        if (enemyFount && unionFound) {
            hitDisksCollector.addAll(hitDisks);
        }
        return enemyFount && unionFound;
    }

    public List<Cell> getAvailableMoves(Disk playerDisk) {
        List<Cell> availableMoves = new ArrayList<>();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                Cell target = Cell.create(x, y);
                if (isCellFree(target) && isValidMove(target, playerDisk)) {
                    availableMoves.add(target);
                }
            }
        }
        return availableMoves;
    }

    public boolean hasPlayerMove(Disk playerDisk) {
        return !getAvailableMoves(playerDisk).isEmpty();
    }

    public boolean isEndOfGame() {
        return !(hasPlayerMove(Disk.WHITE) || hasPlayerMove(Disk.BLACK));
    }

    public Disk getGameWinner() {
        if (getWhiteDisksAmount() == getBlackDisksAmount()) {
            return null;
        } else if (getWhiteDisksAmount() > getBlackDisksAmount()) {
            return Disk.WHITE;
        } else {
            return Disk.BLACK;
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public MatrixIterator<Cell> iterator() {
        return new MatrixIterator<Cell>() {

            @Override
            public int getX() {
                return x;
            }

            @Override
            public int getY() {
                return y;
            }

            @Override
            public Cell current() {
                return fieldMatrix[x][y];
            }

            private int x = -1;
            private int y = 0;

            @Override
            public boolean hasNext() {
                return x + 1 < width || y + 1 < height;
            }

            @Override
            public Cell next() {
                nextCell();
                return fieldMatrix[x][y];
            }

            private void nextCell() {
                x++;
                if (x >= width) {
                    x = 0;
                    y++;
                }
            }
        };
    }

    public interface MatrixIterator<T> extends Iterator<T> {
        int getX();

        int getY();

        T current();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameField gameField = (GameField) o;

        return Arrays.deepEquals(fieldMatrix, gameField.fieldMatrix);

    }

    @Override
    public int hashCode() {
        if (fieldMatrix == null)
            return 0;

        int result = 1;
        for (Cell cell : this) {
            result = 31 * result + (cell.getDisk() == null ? "NULL".hashCode() : cell.getDisk().name().hashCode());
        }

        return result;
    }
}
