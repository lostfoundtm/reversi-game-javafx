package com.lostfound.reversi.util;

import com.lostfound.reversi.model.GameField;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

@Component
public class GameContainer {

    private GameField currentGameField;

    public GameField getCurrentGameField() {
        return currentGameField;
    }

    public void setCurrentGameField(GameField gameField) {
        this.currentGameField = gameField;
    }

    @Lookup("gameField")
    public GameField getNewGameField() {
        return null;
    }
}
