package com.lostfound.reversi.util;

import java.util.List;

public class CommonUtils {

    public static <T> T getCycled(T[] array, int index) {
        if (index < 0) {
            while (index < 0) index += array.length;
        } else if (index >= array.length) {
            while (index < 0) index -= array.length;
        }
        return array[index];
    }

    public static <T> T getCycled(List<T> array, int index) {
        if (index < 0) {
            while (index < 0) index += array.size();
        } else if (index >= array.size()) {
            while (index < 0) index -= array.size();
        }
        return array.get(index);
    }
}
