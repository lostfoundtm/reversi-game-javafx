package com.lostfound.reversi.util;

import com.lostfound.reversi.model.Cell;
import com.lostfound.reversi.model.GameField;
import com.lostfound.reversi.model.GameFieldModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GameUtils {

    @Autowired
    private GameFieldModel gameFieldModel;

    public Cell getCell(double x, double y) {
        return new Cell((int) (x / gameFieldModel.getCellSize()),
                (int) (y / gameFieldModel.getCellSize()));
    }

}
